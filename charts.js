let semesterKeys = document.getElementById('semesterKeys').dataset.semesterkeys;
let semesterValues = document.getElementById('semesterValues').dataset.semestervalues;

let studyFieldKeys = document.getElementById('studyFieldKeys').dataset.studyfieldkeys;
let studyFieldValues = document.getElementById('studyFieldValues').dataset.studyfieldvalues;

let degreeValues = document.getElementById('degreeValues').dataset.degreevalues;
let degreeKeys = document.getElementById('degreeKeys').dataset.degreekeys;

let averagesValue = document.getElementById('averagesValue').dataset.averagesvalue;
let averagesKeys = document.getElementById('averagesKeys').dataset.averageskeys;
console.log(JSON.parse(averagesKeys));

let jsonDataForCharts = document.getElementById('jsonDataForCharts').dataset.jsondataforcharts;
let keysData = document.getElementById('keys').dataset.keys;
let attributes = [JSON.parse(JSON.parse(jsonDataForCharts))];

let allAttributes = JSON.parse(document.getElementById('attributesFound').dataset.attributesfound);
console.log(allAttributes);
// var clickedAttr ={};
// var clickedAttrName ='';
function showClickedChart(attrName, attrJason, attrKeys) {
    // console.log(attrName,attrJason,attrKeys);
    //Set attribute Title
    document.getElementById('attrTitle').innerText = jsUcfirst(attrName) + ' Details';
    barChart = c3.generate({
        bindto: '#chart',
        data: {
            json: [attrJason],
            keys:
                {
                    value: attrKeys,
                },
            type: 'bar',
            types: {
                average: 'bar'
            },
            labels: true,

            names:
                {

                    average: 'Average'
                }
        },
        axis: {
            x: {
                type: 'category',
                label: {
                    text: 'attributes', position: 'outer-center'
                },
            },
            y: {
                label: {
                    text: 'rating', position: 'outer-center'
                }
            }
        },
        title: {
            text: '',
        },
        color: {
            pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
        },
        bar: {
            width: {
                ratio: 0.75
            }
        },
        grid: {
            //x: {show: true},
            //y:{show:true}
        },
        legend: {
            position: 'bottom'
        },
        point: {
            show: false
        },
        zoom: {
            enabled: true
        },
    });
    document.getElementById('showDetails').click();

}

function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// console.log(JSON.parse(JSON.parse(jsonDataForCharts)));
// console.log(JSON.parse(JSON.parse(degreeValues)));
// console.log(JSON.parse(degreeKeys));
// console.log(JSON.parse(JSON.parse(studyFieldValues)));
// console.log(JSON.parse(studyFieldKeys));

let overViewWidthSize = 150;

let dataColumnName = '';
let dataColumnAverage = '';
let averages = [JSON.parse(JSON.parse(averagesValue))];
console.log(Object.values(averages[0]));
let averagesChart = c3.generate({
    bindto: '#averages',
    data: {
        onmouseover: function (d) {
            // console.log(d);
            dataColumnName = d.name
            dataColumnAverage = d.value
        },
        onclick: function (d, el) {
            console.log(d);
            console.log(allAttributes[d.name]);
            // clickedAttr = allAttributes[d.name];
            // clickedAttrName = d.name;
            console.log(d.name, allAttributes[d.name], Object.keys(allAttributes[d.name]));
            showClickedChart(d.name, allAttributes[d.name], Object.keys(allAttributes[d.name]));
        },
        // json: Object.values(averages[0]),
        json: averages,
        keys:
            {
                value: JSON.parse(averagesKeys),
            },
        type: 'bar',
        // types: {
        //     average: 'bar'
        // },
        labels: true,

        names:
            {

                average: 'Average'
            }
    },
    axis: {
        x: {
            type: 'category',
            label: {
                text: 'attributes', position: 'outer-center',
            },
            // categories: Object.values(JSON.parse(averagesKeys)),
        },
        y: {
            label: {
                text: 'rating',// position: 'outer-center'
            }
        }
    },
    title: {
        text:
            'Averages'
    },
    color: {
        pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
    },
    bar: {
        width: {
            ratio: 0.75
        }
    },
    grid: {
        //x: {show: true},
        //y:{show:true}
    },
    legend: {
        position: 'bottom'
    },
    point: {
        show: false
    },
    zoom: {
        enabled: true
    },
    size: {
        width: 800
    },
    tooltip: {
        show: true,
        tooltip: {
            grouped: false
        },
        format: {
            title: function (x, index) {
                return 'Data ' + x;
            }
        },
        contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
            //console.log(d);
            return '<div style="background: ghostwhite;padding-left: 10px;padding-right: 10px"><h1 >' + dataColumnName + ': ' + dataColumnAverage + '</h1></div>';//d
        }
    }
});

let fachsemester = [JSON.parse(JSON.parse(semesterValues))];
let fachsemesterChart = c3.generate({
    bindto: '#fachsemester',
    data: {
        json: fachsemester,
        keys:
            {
                value: JSON.parse(semesterKeys),
            },
        type: 'pie',
        types: {
            average: 'pie'
        },
        labels: true,

        names:
            {

                average: 'Average'
            }
    },
    axis: {
        x: {
            type: 'category',
            label: {
                text: 'attributes', position: 'outer-center'
            },
        },
        y: {
            label: {
                text: 'rating', position: 'outer-center'
            }
        }
    },
    title: {
        text: 'Semester'
    },
    color: {
        pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
    },
    bar: {
        width: {
            ratio: 0.75
        }
    },
    grid: {
        //x: {show: true},
        //y:{show:true}
    },
    legend: {
        position: 'bottom'
    },
    point: {
        show: false
    },
    zoom: {
        enabled: true
    },
    size: {
        width: overViewWidthSize
    }
});

let studiengang = [JSON.parse(JSON.parse(studyFieldValues))];
let studiengangChart = c3.generate({
    bindto: "#studiengang",
    data: {
        json: studiengang,
        keys: {
            value: JSON.parse(studyFieldKeys),
        },
        type: 'pie',
        types: {
            average:
                'pie'
        },
        labels: true,

        names:
            {

                average:
                    'Average'
            }
    },
    axis: {
        x: {
            type:
                'category',
            label: {
                text:
                    'attributes', position: 'outer-center'
            },
        },
        y: {
            label: {
                text:
                    'rating', position: 'outer-center'
            }
        }
    },
    title: {
        text:
            'Study Program'
    },
    color: {
        pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
    },
    bar: {
        width: {
            ratio: 0.75
        }
    },
    grid: {
        //x: {show: true},
        //y:{show:true}
    },
    legend: {
        position:
            'bottom'
    },
    point: {
        show: false
    },
    zoom: {
        enabled: true
    },
    size: {
        width: overViewWidthSize
    }
});

let degree = [JSON.parse(JSON.parse(degreeValues))];
let degreeChart = c3.generate({
    bindto: '#abschluss',
    data: {
        json: degree,
        keys:
            {
                value: JSON.parse(degreeKeys),
            },
        type:
            'pie',
        types: {
            average:
                'pie'
        },
        labels: true,

        names:
            {
                average: 'Average'
            }
    },
    axis: {
        x: {
            type:
                'category',
            label: {
                text:'attributes',
                position: 'outer-center'
            },
        },
        y: {
            label: {
                text:
                    'rating', position: 'outer-center'
            }
        }
    },
    title: {
        text:
            'Degree'
    },
    color: {
        pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
    },
    bar: {
        width: {
            ratio: 0.75
        }
    },
    grid: {
        //x: {show: true},
        //y:{show:true}
    },
    legend: {
        position:
            'bottom'
    },
    point: {
        show: false
    },
    zoom: {
        enabled: true
    },
    size: {
        width: overViewWidthSize
    }
});

// console.log(degree);
var comparisonData = ['Currenct Course Averages'];
comparisonData.concat(Object.values(averages[0]));
// console.log(comparisonData.concat(Object.values(averages[0])));
// console.log(JSON.parse(averagesKeys));
var comparison = c3.generate({
    bindto: "#comparisonChart",
    data: {
        type: 'bar',
        columns: [comparisonData.concat(Object.values(averages[0]))],
        // columns: [],
    },
    axis: {
        x: {
            type: 'category',
            categories: JSON.parse(averagesKeys),
            label: {
                position : 'outer-center'
            }
        }
    }
});
