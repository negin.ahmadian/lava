# InEval (LAVA)

## Description
##### *Insights into Course Evaluation by Interactive Data Visualization (InEval)*
InEval is an online platform which help both side of the educational system (professors and students) to understand better the evaluation of the professor’s Performance through visualizing.


## setup
To have it working you need: 
- A server with PHP and MySQL for windows you can use WAMP (http://www.wampserver.com/en/)

### Install:
To make the application ready to run you need to first run the command (you need to be in the folder to run this command): 
- php composer.phar install 

(Composer is similar to packet managers like npm. In our application it loads the classes we need automatically.
 For more information about composer look it up in Google ;) )

Database configuration can be set in :
- lib/config.php

If you wish to change the data (csv file) you could do so by changing the 
dbname value in config to something new(or drop the old database and keep it's name unchanged) and give the csv file names according 
in index.php function createTablesAndPupulateThem.

 
## Structure

![Screenshot_-_15_08_003](/uploads/c5001408a0d02a667c610796bb64b7cb/Screenshot_-_15_08_003.png)

### Frontend : 
HTML : The html content could be edited in index.phtml

Styles : The styles are kept in styles.css

Charts definition : The structure of charts are to be found in charts.js

### Sending data from backend to frontend (index.phtml) :
You can use $viewPhtml->assign([variable Name in phtml],[variable])
And you can access them in index.phtml by just using the [variable] name.

For sending data from index.phtml to charts.js you can use a div with ID and data attribute.
See line 119 of index.phtml and charts line 2 as an example.

### Libraries used:
We have used PHP, My-Sql, C3.js, Vue.js,JavaScript, HTML and CSS in the implementation of our project

### Screenshots of the visualizations :
#### Visualisation of the target course in generell

![Screenshot_-_20_08](/uploads/72e199c28018641b54b6d9c63daef3c0/Screenshot_-_20_08.png)

#### Visualisation of the target course in details

![Screenshot_-_20_08_002](/uploads/8ca8ac92fa66d76907aee16be8b9b709/Screenshot_-_20_08_002.png)


#### Visualisation of the comparison with another courses or perofessors

![Screenshot_-_20_08_003](/uploads/6e5283f4b77b1afc9dd79ed086895773/Screenshot_-_20_08_003.png)


#### Group Members
Negin Ahmadian

Marco Hellmann

Forough Zarei

Muhammad Zeeshan

