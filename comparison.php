<?php
define('ROOT', __DIR__);
require 'vendor/autoload.php';

$viewPhtml = new View('comparison.phtml');

// Create connection
$database=createTablesAndPupulateThem();

$requestHandler = new requestHandler($database);

$viewPhtml->assign('_GET', $_GET);
//var_dump($requestHandler->getDepartments());exit;
$viewPhtml->assign('departments', $requestHandler->getDepartments());

if (isset($_GET['faculty'])) {
    $viewPhtml->assign('subDepartments', $requestHandler->getSubDepartments($_GET['faculty']));
}

if (isset($_GET['faculty'], $_GET['department'])) {
    $viewPhtml->assign('professors',
        $requestHandler->getProfessors($_GET['faculty'], $_GET['department']));
}

if (isset($_GET['faculty'], $_GET['department'], $_GET['lecturer'])) {
    $viewPhtml->assign('courseName',
        $requestHandler->getCourseName($_GET['faculty'], $_GET['department'], $_GET['lecturer']));
}

if (isset($_GET['faculty'], $_GET['department'], $_GET['lecturer'], $_GET['course_name'])) {
    $viewPhtml->assign('yearSemester',
        $requestHandler->getYearSemester($_GET['faculty'], $_GET['department'], $_GET['lecturer'],
            $_GET['course_name']));
}

if (isset($_GET['faculty'], $_GET['department'], $_GET['lecturer'], $_GET['course_name'], $_GET['year'], $_GET['term'])) {
    $attributes = $requestHandler->getattributes($_GET['faculty'], $_GET['department'], $_GET['lecturer'],
        $_GET['course_name'], $_GET['year'], $_GET['term']);
    $attrParser = new parseAttributes($attributes);
    $attributesFound = $attrParser->groupAttributes();
    $viewPhtml->assign('attributesFound', $attributesFound);
    list($semesterKeys, $semesterValues) = $attrParser->getJsonAttribute('semester');
    list($studyFieldKeys, $studyFieldValues) = $attrParser->getJsonAttribute('study-program');
    list($degreeKeys, $degreeValues) = $attrParser->getJsonAttribute('degree');
    list($averagesKeys, $averagesValue) = $attrParser->getAverages();
    $viewPhtml->assign('ARRAY', array(
        'semesterKeys' => $semesterKeys,
        'semesterValues' => $semesterValues,
        'studyFieldKeys' => $studyFieldKeys,
        'studyFieldValues' => $studyFieldValues,
        'degreeKeys' => $degreeKeys,
        'degreeValues' => $degreeValues,
        'averagesValue' => json_encode($averagesValue),
        'averagesKeys' => $averagesKeys,
    ));
    list($positiveComments,$positiveCommentsJson) = $attrParser->getJsonAttribute('positive');
    list($negativeComments,$negativeCommentsJson) = $attrParser->getJsonAttribute('negative');
//    var_dump($positiveComments);exit;
    $viewPhtml->assign('positiveComments',$positiveComments);
    $viewPhtml->assign('positiveCommentsCounts',(array)json_decode($positiveCommentsJson));
    $viewPhtml->assign('negativeComments',$negativeComments);
    $viewPhtml->assign('negativeCommentsCounts',(array)json_decode($negativeCommentsJson));
}

if (isset($_GET['faculty'], $_GET['department'], $_GET['lecturer'], $_GET['course_name'], $_GET['year'], $_GET['term'], $_GET['attr'])) {
    list($keys, $attributeValues) = $attrParser->getAtrribute($_GET['attr']);
    $jsonDataForCharts = json_encode($attributeValues);
    $viewPhtml->assign('jsonDataForCharts', $jsonDataForCharts);
    $viewPhtml->assign('keys', $keys);
//    var_dump($attrParser->getAverages());EXIT;
}


//echo getRestOfHtml($jsonDataForCharts, $keys);
$database->close();


/**
 * @param mysqli $database
 */
function createTablesAndPupulateThem()
{
    $createTables = new createTables();

    $createAttributes = $createTables->createTableFromCSV("attributes", "part2.csv");
    if ($createAttributes) {
        $createTables->populateTableFromCSV("attributes", "part2.csv");
    }

    $createCourse = $createTables->createTableFromCSV("courseMetaInfo", "part 1.csv");
    if ($createCourse) {
        $createTables->populateTableFromCSV("courseMetaInfo", "part 1.csv");
    }

    return $createTables->getDb();
}

function getAllAttributes($attrParser)
{

}

?>