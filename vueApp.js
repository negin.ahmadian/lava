var attrForCompare = {};
var barChart = {};
let comparisonURL  = "comparison.php";
console.log(comparisonURL);
var app = new Vue({
    el: '#vueApp',
    data: {
        message: 'Hello Vue!',
        showComment: false,
        showDetail: false,
        deps: '',
        dep: '',
        subs: '',
        sub: '',
        profs: '',
        prof: '',
        courses: '',
        course: '',
        yearSems: '',
        year: '',
        term: '',
        attributes: '',
        attr: {},
        attrNameSelected: '',
        showCompare: false,
    },
    methods: {
        toggleComments: function () {
            if (this.showComment == false) {
                this.showComment = true;
            } else {
                this.showComment = false;
            }
        },
        showDetails: function () {
            this.showDetail = true;
        },
        hideDetails: function () {
            this.showDetail = false;
        },
        getDeps: function () {
            this.deps = this.subs = this.profs = this.courses = this.yearSems = '';
            this.dep = this.sub = this.prof = this.course = this.year = this.term = '';
            this.showCompare = true;
            let url = comparisonURL + this.pid;
            console.log('getDeps');
            if (this.pid == null) {
                url = comparisonURL;
            }
            fetch(url, {
                body: JSON.stringify(),
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
            }).then(response => response.json())
                .then((data) => {
                    console.log(this.deps);
                    this.deps = data;
                    console.log(this.deps);

                })
        },
        getSubs: function (dep) {
            this.dep = dep;
            this.subs = this.profs = this.courses = this.yearSems = '';
            this.sub = this.prof = this.course = this.year = this.term = '';
            let url = comparisonURL+"?faculty=" + dep;
            console.log('getsubs');
            fetch(url, {
                body: JSON.stringify(),
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
            }).then(response => response.json())
                .then((data) => {
                    console.log(url);
                    this.subs = data;
                    console.log(this.subs);

                })
        },
        getProfs: function (sub) {
            this.sub = sub;
            this.profs = this.courses = this.yearSems = '';
            this.prof = this.course = this.year = this.term = '';
            let url = comparisonURL+"?faculty=" + this.dep + "&department=" + sub;
            // console.log('getProfs');
            fetch(url, {
                body: JSON.stringify(),
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
            }).then(response => response.json())
                .then((data) => {
                    // console.log(url);
                    // console.log(data);
                    this.profs = data;
                })
        },
        getCourses: function (prof) {
            this.prof = prof;
            this.courses = this.yearSems = '';
            this.course = this.year = this.term = '';
            let url = comparisonURL+"?faculty=" + this.dep + "&department=" + this.sub + "&lecturer=" + this.prof;
            // console.log('getCourses');
            fetch(url, {
                body: JSON.stringify(),
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
            }).then(function (response) {
                return response.json()

            })
                .then((data) => {
                    this.courses = data;
                })
        },
        getYearSems: function (course) {
            this.course = course;
            this.yearSems = '';
            this.year = this.term = '';
            let url = comparisonURL+"?faculty=" + this.dep + "&department=" + this.sub + "&lecturer=" + this.prof + "&course_name=" + this.course;
            // console.log('getYearSems');
            fetch(url, {
                body: JSON.stringify(),
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
            }).then(function (response) {
                return response.json()
            })
                .then((data) => {
                    this.yearSems = data;
                })
        },
        getAttributes: function (year, term) {
            this.year = year;
            this.term = term;
            let url = comparisonURL+"?faculty="
                + this.dep + "&department="
                + this.sub + "&lecturer="
                + this.prof + "&course_name="
                + this.course + "&year="
                + this.course + this.course
                + "&year=" + this.year
                + "&term=" + this.term;
            console.log('getYearSems');
            fetch(url, {
                body: JSON.stringify(),
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
            }).then(function (response) {
                console.log(response);
                return response.json()
            })
                .then((data) => {
                    console.log(data);
                    console.log(url);
                    this.attributes = data;
                    attrForCompare = data;

                })
        },
        setAttribute: function (attr, values) {
            console.log(attr, values);
            this.attr[attr] = values;
            this.attrNameSelected = attr;
            let colValues = [this.course + ' ' + this.year + this.term + '(' + this.prof + ')'];
            colValues = colValues.concat(Object.values(this.attributes));
            console.log(colValues);

            comparison.load({
                columns: [colValues]
            });

        },
        hideCompare: function () {
            this.showCompare = false;
        }


    }
})