<?php
/**
 * Created by PhpStorm.
 * User: Negin Khanome Gole
 * Date: 23.06.2019
 * Time: 13:05
 */

class parseAttributes{

    public $attributes;
    /**
     * parseAttributes constructor.
     * @param array $attributes
     */
    public function __construct($attributes)
    {
        $this->attributes= $attributes;
    }

    public function groupAttributes(){
        $groupedAttr = array();
        foreach ($this->attributes as $attr =>$value ){
            $nameLenght=strpos($attr,'_')?strpos($attr,'_'):-1;
            $attrName= substr($attr,0,$nameLenght);
            $attrSubName = str_replace($attrName.'_','',$attr);
            $groupedAttr[$attrName][$attrSubName]=$value;
        }
        return $groupedAttr;
    }

    public function getAtrribute($name){
        $entries=array();
        $counts=array();
        $attrbutes = $this->groupAttributes();
        if (key_exists($name,$attrbutes)){
            $attribute = $attrbutes[$name];
            foreach ($attribute as $key =>$value){
                //adding a space to the end of the value for fixing the problme with c3 that the key was read as int!!!!
                $changedKey = str_replace($name.'_','',$key).' ';


                unset($attribute[$key]);
                $attribute[$changedKey]=$value;
                if ($value==99){
                    unset($attribute[$changedKey]);
                }
                // To parse the entry count attributes
                if (strpos($key,'entry')!==false && $value!=99){
                    $entries []=$value;
                }
                if (strpos($key,'count')!==false && $value!=99){
                    $counts []=$value;
                }
            }

            $keys = array_keys($attribute);
            if (count($entries)>0 && count($entries)==count($counts)){
                $keys = $entries;
                $attribute = array_combine($entries,$counts);
            }
            return array($keys,$attribute );
        }
        else{
            return false;
        }
    }

    public function getJsonAttribute($attributeName)
    {
        list($keys, $values) = $this->getAtrribute($attributeName);
        return array($keys, json_encode($values));
    }

    public function getAverages(){
        $averages = array();
        $averagesGrouped = array();
        foreach($this->attributes as $key =>$value){
            if ($value==99){
                continue;
            }
            if (strpos($key,'average')){
                $key = str_replace('_average','',$key);
                $averages [$key]=number_format((float)$value, 2, '.', ''); ;
            }
        }
        return array(array_keys($averages),$averages);
    }



}