<?php
/**
 * Created by PhpStorm.
 * User: Negin Khanome Gole
 * Date: 21.06.2019
 * Time: 20:48
 */

class createTables
{
    /**
     * @var mysqli
     */
    protected $db ;

    /**
     * createTables constructor.
     * @param mysqli $db
     */
    public function __construct()
    {
        $db = $this->createDB();
        $this->db = $db;
    }

    /**
     * @return bool|mysqli
     */
    public function getDb(){
        return $this->db;
    }

    /**
     * @param $tableName
     * @param $csvFileName
     */
    public function createTableFromCSV($tableName, $csvFileName)
    {
        if($this->checkIfTableExists($tableName)){
            return false;
        }
        $attributesFile = fopen($csvFileName, "r");
        $header = fgetcsv($attributesFile);
        $columnNameType = '';
        foreach ($header as $column) {
            if ($column === "Course_ID") {
                $columnNameType .= '`'.$column .'`'. ' int(60) NOT NULL Primary key,' . PHP_EOL;
                continue;
            }
            if (strpos($column, '.') !== false) {
                $column = str_replace('.', '_', $column);
            }
            if (strpos($column, ' ') !== false) {
                $column = str_replace(' ', '_', $column);
            }
            $columnNameType .= '`'.$column .'`'. ' text NOT NULL ,' . PHP_EOL;
        }
//        var_dump($columnNameType);
        $columnNameType = rtrim($columnNameType, ',' . PHP_EOL);
        $createAttributeTable = "CREATE TABLE if not exists " . $tableName . " (" . $columnNameType . ") DEFAULT CHARSET=utf8";
//print_r($sql);
        if ($this->db->query($createAttributeTable) === TRUE) {
            return true;
        } else {
            echo "Error creating table: " . $this->db->error;
            return false;
        }
    }

    /**
     * @param $tableName
     * @param $csvFile
     */
    public function populateTableFromCSV($tableName, $csvFile)
    {
        $importCsvToDB = "LOAD DATA LOCAL INFILE '" . $csvFile . "' 
                            INTO TABLE " . $tableName . " 
                            FIELDS TERMINATED BY ',' 
                            ENCLOSED BY '\"'
                            LINES TERMINATED BY '\n'
                            IGNORE 1 LINES";
        if ($this->db->query($importCsvToDB) === TRUE) {
//            print_r($this->db);
            echo "Table inserted successfully";
            return true;
        } else {
            echo "Error insert table: " . $this->db->error;
            return false;
        }

    }

    private function checkIfTableExists($tablseName)
    {
        $checkQuery = 'SELECT 1 FROM '.$tablseName.' LIMIT 1';
        if ($result=$this->db->query($checkQuery)){
            return true;
        }
        else {
            return false;
        }
    }

    private function createDB(){
        $config = new config();
        $servername = $config->getConfig('servername');
        $username = $config->getConfig('username');
        $password = $config->getConfig('password');
        $dbname = $config->getConfig('dbname');
        $db  = new mysqli($servername, $username, $password);
        if ($db === false){
            echo 'CONNECTION TO DB FAILED';
            return false;
        }
        $result = $db->query('create database if not exists '.$dbname);
        if($result===false){
            echo 'Creating database failed';
            return false;
        }
        $db->close();
        $db  = new mysqli($servername, $username, $password,$dbname);

        return $db;
    }
}





