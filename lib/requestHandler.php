<?php
/**
 * Created by PhpStorm.
 * User: Negin Khanome Gole
 * Date: 21.06.2019
 * Time: 21:41
 */

class requestHandler
{
    /**
     * @var mysqli
     */
    protected $db;

    /**
     * @var
     */
    protected $request;

    /**
     * requestHandler constructor.
     * @param mysqli $db
     * @param $request
     */
    public function __construct($db, $request = null)
    {
        $this->db = $db;
        $this->request = $request;
    }


    public function getDepartments()
    {
        $depatmentsQuery = "SELECT distinct faculty FROM courseMetaInfo";
        $result = $this->db->query($depatmentsQuery);
        $deps = array();
        if ($result->num_rows > 0) {
            // output data of each row
//            echo '<option value="index.php">Please Select</option>';
            while ($row = $result->fetch_assoc()) {
                if(isset($_GET['faculty'])){
                    if ($row["faculty"]==$_GET['faculty']){
                        $selected ='selected';
                    }else{
                        $selected='';
                    }
                }
                $deps []= $row["faculty"];
//                echo " <option $selected value='?faculty=".$row["faculty"]."'>" . $row["faculty"] . "</option><br>";
            }
        } else {
            echo "0 results";
        }
        return $deps;
    }

    public function getSubDepartments($department)
    {
        $subdepatmentsQuery = "SELECT distinct department FROM courseMetaInfo where faculty = '" . $department . "'";
        $result = $this->db->query($subdepatmentsQuery);
        $subDeps=array();
        if ($result) {
            if ($result->num_rows > 0) {
                // output data of each row
//                echo '<option value="">Please Select</option>';
                while ($row = $result->fetch_assoc()) {
                    if(isset($_GET['department'])){
                        if ($row["department"]==$_GET['department']){
                            $selected ='selected';
                        }else{
                            $selected='';
                        }
                    }
                    $subDeps [] = $row["department"];
//                    echo "  <option $selected value='?faculty=".$department."&department=".$row["department"]."'>" . $row["department"] . "</option><br>";
                }
            } else {
                echo "0 results";
            }
        } else {
            echo $this->db->error;
        }
        return $subDeps;
    }


    public function getProfessors( $department,$subdepartment)
    {
        $professorsQuery = "SELECT distinct lecturer FROM courseMetaInfo 
                                            where faculty = '" . $department . "'
                                                and department = '" . $subdepartment . "' ";
        $result = $this->db->query($professorsQuery);
        $profs = array();
        if ($result) {
            if ($result->num_rows > 0) {
                // output data of each row

                while ($row = $result->fetch_assoc()) {
                    if(isset($_GET['lecturer'])){
                        if ($row["lecturer"]==$_GET['lecturer']){
                            $selected ='selected';
                        }else{
                            $selected='';
                        }
                    }
                    $profs [] = $row["lecturer"];
//                    echo "<option $selected value='?faculty=".$department."&department=".$subdepartment."&lecturer=".$row["lecturer"]."'>" . $row["lecturer"] . "</option><br>";
                }
            } else {
                echo "0 results";
            }
        } else {
            echo $this->db->error;
        }
        return $profs;
    }


    public function getCourseName($faculty, $subdepartment, $professor)
    {
        $coursesQuery = "SELECT distinct course_ID , course_name FROM courseMetaInfo 
                              where 
                                 lecturer = '" . $professor . "' 
                                 and faculty = '" . $faculty . "' 
                                 and department = '" . $subdepartment . "' ";
        $result = $this->db->query($coursesQuery);
        $courseNames = array();
        if ($result) {
            if ($result->num_rows > 0) {
                // output data of each row
//                echo '<option value="">Please Select</option>';
                while ($row = $result->fetch_assoc()) {
                    if(isset($_GET['course_name'])){
                        if ($row["course_name"]==$_GET['course_name']){
                            $selected ='selected';
                        }else{
                            $selected='';
                        }
                    }
                    $courseNames []=$row["course_name"];
//                    echo "<option $selected value='?faculty=".$faculty."&department=".$subdepartment."&lecturer=".$professor."&course_name=".$row["course_name"]."'>" . $row["course_name"] . "</option><br>";
                }
            } else {
                echo "0 results";
            }
        } else {
            echo $this->db->error;
        }
        return $courseNames;
    }

    public function getYearSemester($faculty, $subdepartment, $professor,$course_name)
    {
        $semesterYearQuery = "SELECT distinct term , `year` FROM courseMetaInfo 
                                                where course_name = '" . $course_name . "' and lecturer = '" . $professor . "' 
                                 and faculty = '" . $faculty . "' 
                                 and department = '" . $subdepartment . "' ";
        $result = $this->db->query($semesterYearQuery);
        $yearSem = array();
        if ($result) {
            if ($result->num_rows > 0) {
                // output data of each row
//                echo '<option value="">Please Select</option>';
                while ($row = $result->fetch_assoc()) {
                    if(isset($_GET['term'],$_GET['year'])){
                        if ($row["term"]==$_GET['term'] && $row["year"]==$_GET['year']){
                            $selected ='selected';
                        }else{
                            $selected='';
                        }
                    }
                    $yearSem[]=$row;
//                    echo "<option $selected value='?faculty=".$faculty."&department=".$subdepartment."&lecturer=".$professor."&course_name=".$course_name."&term=".$row["term"]."&year=".$row["year"]."'>" . $row["term"].$row["year"] . "</option><br>";
                }
            } else {
                echo "0 results";
            }
        } else {
            echo $this->db->error;
        }
        return $yearSem;
    }

    public function getattributes($faculty, $subdepartment, $professor,$course_name,$year, $term)
    {
        $semesterYearQuery = "SELECT attributes.* FROM attributes join courseMetaInfo on attributes.course_ID = courseMetaInfo.course_Id
                                                where `year` = '" . $year . "' 
                                                and  term = '" . $term . "' 
                                                and course_name = '" . $course_name . "' 
                                                and lecturer = '" . $professor . "'
                                                and faculty = '" . $faculty . "'
                                                and department = '" . $subdepartment . "' ";
//        var_dump($semesterYearQuery);
        $result = $this->db->query($semesterYearQuery);
        if ($result) {
            if ($result->num_rows > 0) {
                // output data of each row
//                while ($row = $result->fetch_assoc()) {
//                    foreach ($row as $attribute => $value) {
//                        echo $attribute . ' ' . $value . "<br>";
//                    }
//                    //echo " Name: " . $row["studiengang_1"] . " " . $row["year"] . "<br>";
//                }
                $attributes = $result->fetch_all(MYSQLI_ASSOC);
                return $attributes[0] ;
            } else {
                echo "0 results";
            }
        } else {
            echo $this->db->error;
        }
    }

}