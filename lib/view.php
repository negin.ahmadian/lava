<?php


class View
{
    private $data = array();

    /**
     * @var bool|string file path
     */
    private $render = FALSE;

    /**
     * View constructor.
     * @param string $template
     */
    public function __construct($template)
    {
        try {
            $file = ROOT . '/' . strtolower($template) ;

            if (file_exists($file)) {
                $this->render = $file;
            } else {
                throw new Exception('Template ' . $template . ' not found!');
            }
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param string $variable
     * @param mixed $value
     */
    public function assign($variable, $value)
    {
        if ($variable=='ARRAY'){
            foreach ($value as $name =>$var)
            {
                $this->assign($name,$var);
            }
        }else{
            $this->data[$variable] = $value;
        }

    }

    public function __destruct()
    {
        extract($this->data);
        include($this->render);

    }
}
